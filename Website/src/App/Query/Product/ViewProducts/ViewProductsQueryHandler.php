<?php 

    namespace CQRS\App\Query\Product\ViewProducts;

    use CQRS\Core\Domain\Model\Product\ProductService;

    final class ViewProductsQueryHandler {

        private $product_service;
        private $product_query;

        public function __construct(ProductService $product_service, ViewProductsQuery $product_query) {

            $this->product_service = $product_service;
            $this->product_query = $product_query;

        }

        public function handle() {

            $request = $this->product_query->getRequest();
            $this->product_service->execute([], $request);

        }

    }