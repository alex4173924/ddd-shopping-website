<?php

    namespace CQRS\Core\Domain\Model\Product;

    final class Product implements ProductModelInterface {

        private $id;
        private $user_id;
        private $publication_date;
        private $title;
        private $description;

        public function __construct(array $product_data) {

            $this->id = $product_data['id'] ?? '';
            $this->user_id = $product_data['user_id'] ?? '';
            $this->publication_date = $product_data['publication_date'] ?? '';
            $this->title = $product_data['title'] ?? '';
            $this->description = $product_data['description'] ?? '';

        }

        #[\Override]
        public function getProductId(): string {

            return $this->id;

        }

        #[\Override]
        public function getUserId(): string {

            return $this->user_id;

        }

        #[\Override]
        public function getPublicationDate(): string {

            return $this->publication_date;

        }

        #[\Override]
        public function getTitle(): string {

            return $this->title;

        }

        #[\Override]
        public function getDescription(): string {

            return $this->description;

        }

        #[\Override]
        public function index(array $products): void {
            
            $success_mesasge = $_GET['success'] ?? '';
            $error_message = $_GET['error'] ?? '';
            $products_json = json_encode($products);

            $query_string = 'products=' . urlencode($products_json) . '&success=' . urlencode($success_mesasge) . '&error=' . urlencode($error_message);

            header('Location:src/View/Products/index.php?' . $query_string);
            exit();

        }

        #[\Override]
        public function storeSuccess(): void {

            $success_message = 'The item has been added to the cart!';
            $controller = 'ViewProductsQueryHandler';
            $request = 'index';
            $method = 'handle';

            $query_string = 'controller=' . urlencode($controller) . '&request=' . urlencode($request) . '&method=' . urlencode($method) . '&success=' . urlencode($success_message);

            header('Location:index.php?' . $query_string);
            exit();

        }

        #[\Override]
        public function storeError(): void {

            $error_message = 'The item title has already been taken!';
            $query_string = 'error=' . urlencode($error_message);

            header('Location:src/View/Products/create.php?' . $query_string);
            exit();

        }

        #[\Override]
        public function showSuccess(array $product_info): void {

            $product = json_encode($product_info);

            $query_string = 'product=' . urlencode($product);

            header('Location:src/View/Products/show.php?' . $query_string);
            exit();

        }

        #[\Override]
        public function showError(): void {

            $error_message = 'Couldn`t find the item you are looking for!';
            $controller = 'ViewProductsQueryHandler';
            $request = 'index';
            $method = 'handle';

            $query_string = 'controller=' . urlencode($controller) . '&request=' . urlencode($request) . '&method=' . urlencode($method) . '&error=' . urlencode($error_message);

            header('Location:index.php?' . $query_string);
            exit();

        }

        #[\Override]
        public function editSuccess(array $product_info): void {

            $product = json_encode($product_info);
            $query_string = 'product=' . urldecode($product);

            header('Location:src/View/Products/edit.php?' . $query_string);
            exit();

        }

        #[\Override]
        public function editError(): void {

            $error_message = 'No such item with the id provided!';
            $controller = 'ViewProductsQueryHandler';
            $request = 'index';
            $method = 'handle';

            $query_string = 'controller=' . urlencode($controller) . '&request=' . urlencode($request) . '&method=' . urlencode($method) . '&error=' . urlencode($error_message);

            header('Location:index.php?' . $query_string);
            exit();

        }

        #[\Override]
        public function updateSuccess(): void {

            $success_message = 'You have updated the product successfully!';
            $controller = 'ViewProductsQueryHandler';
            $request = 'index';
            $method =  'handle';

            $query_string = 'controller=' . urlencode($controller) . '&request=' . urlencode($request) . '&method=' . urlencode($method) . '&success=' . urlencode($success_message);

            header('Location:index.php?' . $query_string);
            exit();

        }

        #[\Override]
        public function updateError(): void {

            $error_message = 'No such item with the id provided!';
            $controller = 'ViewProductsQueryHandler';
            $request = 'index';
            $method = 'handle';

            $query_string = 'controller=' . urlencode($controller) . '&request=' . urlencode($request) . '&method=' . urlencode($method) . '&error=' . urlencode($error_message);

            header('Location:index.php?' . $query_string);
            exit();

        }

        #[\Override]
        public function deleteSuccess(): void {

            $success_mesasge = 'The product has been deleted successfully!';
            $controller = 'ViewProductsQueryHandler';
            $request = 'index';
            $method = 'handle';

            $query_string = 'controller=' . urlencode($controller) . '&request=' . urlencode($request) . '&method=' . urlencode($method) . '&success=' . urlencode($success_mesasge);

            header('Location:index.php?' . $query_string);
            exit();

        }

        #[\Override]
        public function deleteError(): void {

            $error_message = 'No such item with the id provided!';
            $controller = 'ViewProductsQueryHandler';
            $request = 'index';
            $method = 'handle';

            $query_string = 'controller=' . urlencode($controller) . '&request=' . urlencode($request) . '&method=' . urlencode($method) . '&error=' . urlencode($error_message);
            
            header('Location:index.php?' . $query_string);
            exit();

        }

        
    }