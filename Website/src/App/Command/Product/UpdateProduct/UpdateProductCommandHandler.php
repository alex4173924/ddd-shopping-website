<?php

    namespace CQRS\App\Command\Product\UpdateProduct;

    use CQRS\Core\Domain\Model\Product\ProductService;

    final class UpdateProductCommandHandler {

        private $product_service;
        private $product_command;

        public function __construct(ProductService $product_service, UpdateProductCommand $product_command) {

            $this->product_service = $product_service;
            $this->product_command = $product_command;

        }

        public function handle() {

            $request = $this->product_command->getRequest();

            $product_data = $this->product_command->getProductData();

            $this->product_service->execute($product_data, $request);

        }
    }