<?php

    namespace CQRS\App\Command\User\RegisterUser;

    use CQRS\Core\Infrastructure\HashPassword\HashPassword;

    final class RegisterUserCommand {

        private $request;
        private $name;
        private $surname;
        private $password;
        private $email;
        private $phone;
        private $city;
        private $hash_password;

        public function __construct(array $credentials, $request) {

            $this->request= $request;
            $this->name = $credentials['name'] ?? '';
            $this->surname = $credentials['surname'] ?? '';
            $this->password = $credentials['password'] ?? '';
            $this->email = $credentials['email'] ?? '';
            $this->phone = $credentials['phone'] ?? '';
            $this->city = $credentials['city'] ?? '';

            $this->hash_password = new HashPassword();

        }

        public function getRequest() {

            return $this->request;

        }

        public function getUserData() {

            return [

                'name' => $this->name,
                'surname' => $this->surname,
                'email' => $this->email,
                'phone' => $this->phone,
                'city' => $this->city,
                'password' => $this->hash_password->hash($this->password)

            ];

        }

    }