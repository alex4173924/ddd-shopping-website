<?php

    namespace CQRS\Core\Domain\Model\User;

    interface UserModelInterface {

        public function getId(): string;

        public function getName(): string;

        public function getSurname(): string;

        public function getEmail(): string;

        public function getPhone(): string;

        public function getCity(): string;

        public function getPassword(): string;

        public function login(): void;

        public function loginError(): void;

        public function register(): void;

        public function registerError(): void;

        public function editSuccess(array $user_data): void;

        public function editError(): void;

        public function updateSuccess(): void;

        public function updateError(): void;

        public function logoutSuccess(): void;

        public function logoutError(): void;
        
    }