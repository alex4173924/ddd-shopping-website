<?php

    namespace CQRS\Core\Domain\Model\Product;

    interface ProductRepositoryInterface {

        public function findById(string $id): ?Product;

        public function getAllProducts(): array;

        public function checkProduct(string $title): bool;

        public function update(array $product): void;

        public function save(Product $product): void;

        public function delete(string $id): void;

    }