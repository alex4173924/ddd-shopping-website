<?php

    namespace CQRS\Factory\FactoryHandlers\Product;

    use CQRS\Core\Domain\Model\Product\ProductService;
    use CQRS\App\Query\Product\EditProduct\EditProductQuery;
    use CQRS\App\Query\Product\EditProduct\EditProductQueryHandler;
    use CQRS\Core\Infrastructure\Repository\ProductRepository\ProductRepository;
    use CQRS\Core\Infrastructure\Database\Database;

    final class EditProductFactory {

        public static function edit(string $request, Database $db) {

            $product_respository = new ProductRepository($db);
            $product_service = new ProductService($product_respository);
            $id = $_GET['id'] ?? '';
            $product_command = new EditProductQuery($id, $request);

            return new EditProductQueryHandler($product_service, $product_command);

        }
    }