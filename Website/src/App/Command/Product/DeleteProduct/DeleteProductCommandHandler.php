<?php

    namespace CQRS\App\Command\Product\DeleteProduct;

    use CQRS\Core\Domain\Model\Product\ProductService;

    final class DeleteProductCommandHandler {

        private $product_service;
        private $product_command;

        public function __construct(ProductService $product_service, DeleteProductCommand $product_command) {

            $this->product_service = $product_service;
            $this->product_command = $product_command;
  
        }

        public function handle() {

            $request = $this->product_command->getRequest();

            $product_data = [
                'id' => $this->product_command->getId()
            ];

            $this->product_service->execute($product_data, $request);

        }

    }