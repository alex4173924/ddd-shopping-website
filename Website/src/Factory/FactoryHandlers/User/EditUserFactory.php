<?php

    namespace CQRS\Factory\FactoryHandlers\User;

    use CQRS\Core\Domain\Model\User\UserService;
    use CQRS\App\Query\User\EditUser\EditUserQuery;
    use CQRS\App\Query\User\EditUser\EditUserQueryHandler;
    use CQRS\Core\Infrastructure\Repository\UserRepository\UserRepository;
    use CQRS\Core\Infrastructure\Database\Database;

    final class EditUserFactory {

        public static function edit(string $request, Database $db) {

            $user_repository = new UserRepository($db);
            $user_service = new UserService($user_repository);
            $edit_command = new EditUserQuery($request);

            return new EditUserQueryHandler($user_service, $edit_command);
            
        }

    }

