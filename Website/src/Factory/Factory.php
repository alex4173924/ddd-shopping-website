<?php

    namespace CQRS\Factory;

    use CQRS\Core\Infrastructure\Database\Database;
    use CQRS\Factory\FactoryHandlers\Home\HomeFactory;
    use CQRS\Factory\FactoryHandlers\Home\HomeFactoryNotFound;
    use CQRS\Factory\FactoryHandlers\User\LoginUserFactory;
    use CQRS\Factory\FactoryHandlers\User\RegisterUserFactory;
    use CQRS\Factory\FactoryHandlers\User\EditUserFactory;
    use CQRS\Factory\FactoryHandlers\User\UpdateUserFactory;
    use CQRS\Factory\FactoryHandlers\User\LogoutUserFactory;
    use CQRS\Factory\FactoryHandlers\Product\ViewProductFactory;
    use CQRS\Factory\FactoryHandlers\Product\AddProductFactory;
    use CQRS\Factory\FactoryHandlers\Product\ShowProductFactory;
    use CQRS\Factory\FactoryHandlers\Product\EditProductFactory;
    use CQRS\Factory\FactoryHandlers\Product\UpdateProductFactory;
    use CQRS\Factory\FactoryHandlers\Product\DeleteProductFactory;

    final class Factory {

        private $controller;
        private $request;
        private $db;

        public function __construct($path) {

            $this->controller = $path['controller'];
            $this->request = $path['request'];
            $this->db = new Database();

        }

        public function createPath() {

            switch($this->controller) {
                
                case 'HomeQueryHandler':

                    return HomeFactory::home($this->request);
                
                case 'LoginUserQueryHandler':

                    return LoginUserFactory::login($this->request, $this->db);             

                case 'RegisterUserCommandHandler':

                    return RegisterUserFactory::register($this->request, $this->db);                

                case 'EditUserQueryHandler':

                    return EditUserFactory::edit($this->request, $this->db);

                case 'UpdateUserCommandHandler':

                    return UpdateUserFactory::update($this->request, $this->db);

                case 'LogoutUserQueryHandler':

                    return LogoutUserFactory::logout($this->request, $this->db);

                case 'ViewProductsQueryHandler':

                    return ViewProductFactory::view($this->request, $this->db);

                case 'AddProductCommandHandler':

                    return AddProductFactory::add($this->request, $this->db);

                case 'ShowProductQueryHandler':

                    return ShowProductFactory::show($this->request, $this->db);

                case 'EditProductQueryHandler':

                    return EditProductFactory::edit($this->request, $this->db);

                case 'UpdateProductCommandHandler':

                    return UpdateProductFactory::update($this->request, $this->db);

                case 'DeleteProductCommandHandler':

                    return DeleteProductFactory::delete($this->request, $this->db);

                default:

                    return HomeFactoryNotFound::notFound();
                
            }
        }
    }