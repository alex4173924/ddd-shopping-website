<?php

    namespace CQRS\Core\Infrastructure\Repository\UserRepository;

    use CQRS\Core\Domain\Model\User\User;
    use CQRS\Core\Domain\Model\User\UserRepositoryInterface;
    use CQRS\Core\Infrastructure\Database\Database;

    final class UserRepository implements UserRepositoryInterface {

        private $db;

        public function __construct(Database $db) {

            $this->db = $db;
        
        }

        #[\Override]
        public function findById(string $id): ?User {

            $table = 'users';

            $sql = "SELECT 
                        id,
                        name,
                        surname,
                        email,
                        phone,
                        city,
                        password
                    FROM $table
                    WHERE id='$id'";

            $result = $this->db->query($sql);

            if($result->num_rows > 0) {

                $user = $result->fetch_assoc();
                
                return new User([
                    'id' => $user['id'],
                    'name' => $user['name'],
                    'surname' => $user['surname'],
                    'email' => $user['email'], 
                    'phone' => $user['phone'],
                    'city' => $user['city'],
                    'password' => $user['password']
                ]);
            }

            return new User([]);
        }

        #[\Override]
        public function findByEmail(string $email): ?User {

            $table = 'users';
            $sql = "SELECT 
                        id,
                        email,
                        password
                    FROM $table
                    where email='$email'";

            $result = $this->db->query($sql);

            if($result->num_rows > 0) {

                $user = $result->fetch_assoc();

                return new User([
                    'id' => $user['id'],
                    'email' => $user['email'], 
                    'password' => $user['password']
                ]);
            }

            return null;

        }

        #[\Override] 
        public function checkCredentials(array $credentials): bool {

            $email = $credentials['email'];
            $phone = $credentials['phone'];
            $table = 'users';

            $sql = "SELECT id
                    FROM $table
                    WHERE email='$email'
                        OR phone='$phone'";

            $result = $this->db->query($sql);

            if($result->num_rows > 0) {
                
                return false;
            
            }

            return true;

        }

        #[\Override]
        public function update(array $user, string $id): void {

            $table = 'users';

            $sql = "UPDATE $table
                    SET 
                        name='{$user['name']}',
                        surname='{$user['surname']}', 
                        email='{$user['email']}',
                        phone='{$user['phone']}',
                        city='{$user['city']}'
                    WHERE id=$id";

            if(!$this->db->query($sql)) {

                die('Unable to update record with id: ' . $id);
                
            }

        }

        #[\Override]
        public function save(User $user): void {

            $table = 'users';
            $sql = "INSERT
                        INTO $table(
                            name,
                            surname,
                            email,
                            phone,
                            city,
                            password
                        ) VALUES(
                            '{$user->getName()}',
                            '{$user->getSurname()}',
                            '{$user->getEmail()}',
                            '{$user->getPhone()}',
                            '{$user->getCity()}',
                            '{$user->getPassword()}'
                        )";

            if(!$this->db->query($sql)) {
                die('Unable to insert records into the database!');
            }

        }

    }
