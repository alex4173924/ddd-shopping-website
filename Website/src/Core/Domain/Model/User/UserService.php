<?php

    namespace CQRS\Core\Domain\Model\User;

    if(session_status() !== PHP_SESSION_ACTIVE) {

        session_start();

    }

    final class UserService {

        private $user_repository;

        public function __construct(UserRepositoryInterface $user_repository) {

            $this->user_repository = $user_repository;
            
        }

        public function execute(array $user_data, string $request) {

            switch($request) {

                case 'login':

                    $email = $user_data['email'];
                    $password = $user_data['password'];

                    $user = $this->user_repository->findByEmail($email);
    
                    if ($user && password_verify($password, $user->getPassword())) {
                        
                        $user->login();

                    }

                    $user = new User([]);
                    $user->loginError();
                
                    break;
                
                case 'register':

                    $user = new User($user_data);

                    if($this->user_repository->checkCredentials([
                            
                            'email' => $user->getEmail(), 
                            'phone' => $user->getPhone()
                        
                        ])) {

                            $this->user_repository->save($user);
                            $user->register();
                    }
                    
                    $user->registerError();

                    break;

                case 'edit':

                    $user = $this->user_repository->findById($_SESSION['user_id']);

                    if($user->getId() !== '') {

                        $user_data = [

                            'name' => $user->getName(),
                            'surname' => $user->getSurname(),
                            'email' => $user->getEmail(),
                            'phone' => $user->getPhone(),
                            'city' => $user->getCity()

                        ];

                        $user->editSuccess($user_data);

                    }

                    $user->editError();

                    break;

                case 'update':

                    $user = $this->user_repository->findById($_SESSION['user_id']);

                    if($user->getId() !== '') {

                        $this->user_repository->update($user_data, $_SESSION['user_id']);
                        $user->updateSuccess();

                    }

                    $user->updateError();

                    break;


                case 'logout':

                    if(session_status() !== PHP_SESSION_ACTIVE) {

                        session_start();

                    }

                    $user = $this->user_repository->findById($_SESSION['user_id']);
                    
                    if($user->getId() !== '') {

                        $user->logoutSuccess();

                    }

                    $user->logoutError();

                    break;

            }
        }

    }