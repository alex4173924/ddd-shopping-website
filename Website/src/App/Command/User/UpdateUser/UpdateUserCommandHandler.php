<?php

    namespace CQRS\App\Command\User\UpdateUser;

    use CQRS\Core\Domain\Model\User\UserService;

    final class UpdateUserCommandHandler {

        private $user_service;
        private $user_command;

        public function __construct(UserService $user_service, UpdateUserCommand $user_command) {

            $this->user_service = $user_service;
            $this->user_command = $user_command;

        }

        public function handle() {

            $request = $this->user_command->getRequest();

            $user_data = $this->user_command->getUserData();

            $this->user_service->execute($user_data, $request);

        }
    }