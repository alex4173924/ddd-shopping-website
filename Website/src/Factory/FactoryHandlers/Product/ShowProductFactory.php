<?php

    namespace CQRS\Factory\FactoryHandlers\Product;

    use CQRS\Core\Domain\Model\Product\ProductService;
    use CQRS\App\Query\Product\ShowProduct\ShowProductQuery;
    use CQRS\App\Query\Product\ShowProduct\ShowProductQueryHandler;
    use CQRS\Core\Infrastructure\Repository\ProductRepository\ProductRepository;
    use CQRS\Core\Infrastructure\Repository\UserRepository\UserRepository;
    use CQRS\Core\Infrastructure\Database\Database;

    final class ShowProductFactory {

        public static function show(string $request, Database $db) {

            $product_respository = new ProductRepository($db);
            $user_repository = new UserRepository($db);
            $product_service = new ProductService($product_respository, $user_repository);
            $id = $_GET['id'] ?? '';
            $product_command = new ShowProductQuery($id, $request);

            return new ShowProductQueryHandler($product_service, $product_command);

        }

    }