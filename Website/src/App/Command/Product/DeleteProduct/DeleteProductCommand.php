<?php

    namespace CQRS\App\Command\Product\DeleteProduct;

    final class DeleteProductCommand {

        private $request;
        private $id;

        public function __construct(string $id, string $request) {

            $this->request = $request;
            $this->id = $id;

        }

        public function getRequest() {

            return $this->request;

        }

        public function getId() {

            return $this->id;

        }

    }