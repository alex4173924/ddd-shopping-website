<?php

    namespace CQRS\App\Query\Product\ShowProduct;

    final class ShowProductQuery {

        private $reqeust;
        private $id;

        public function __construct(string $id, string $request) {

            $this->request = $request;
            $this->id = $id;

        }

        public function getRequest() {

            return $this->request;

        }

        public function getId() {

            return $this->id;

        }
    }