<?php

require_once '../includes/header.php';


    if(isset($_SESSION['user_id'])) {
    
        if(!empty($_GET['error'])) {
            echo "<h4 style='color: red'>" . $_GET['error'] . "</h4><br>";
            $_GET['error'] = null;
        } else {

            echo "<h1 class='edit_h1'>Edit profile</h1>";

        }

        $user = isset($_GET['user_data']) ? json_decode($_GET['user_data']) : null;

        if($user != null) {

?>
    <hr class="hr">


    <form method="POST" style=" background-color: rgba(240, 255, 240, 0.5)" action="../../../index.php?controller=UpdateUserCommandHandler&request=update&method=handle">
        <label for="edit_name">Name:</label><br>
        <input type="text" name="edit_name" id="edit_name" value="<?= $user->name ?>"><br><br>

        <label for="edit_surname">Surname:</label><br>
        <input type="text" name="edit_surname" id="edit_surname" value="<?= $user->surname ?>"><br><br>

        <label for="edit_email">Email:</label><br>
        <input type="text" name="edit_email" id="edit_email" value="<?= $user->email ?>"><br><br>

        <label for="edit_phone">Phone number:</label><br>
        <input type="text" name="edit_phone" id="edit_phone" value="<?= $user->phone ?>"><br><br>

        <label for="edit_city">City:</label><br>
        <input type="text" name="edit_city" id="edit_city" value="<?= $user->city ?>"><br><br>

        <input type="submit" name="edit_button" value="Edit" class="submit_btn">
    </form>

    <div style="display: flex; justify-content: center; align-items: center; height: 15vh;">
            <a href="../../../index.php"><button style=" width: 100%; padding: 10px;padding-right: 50px;
                    padding-left: 50px; background-color: #297fb1;
                    color: white; border: none; margin-top: 5px; border-radius: 20px;">Back</button></a>
        </div>

    <?php } else { ?>

            <h1 class="edit_h1">Something went wrong. No data for the user!</h1>
        <?php }
    
        } else { ?>
        
        <h1 class="edit_h1">Login to your profile first</h1>

    <?php } ?>

<?php require_once '../includes/footer.php'; ?>
