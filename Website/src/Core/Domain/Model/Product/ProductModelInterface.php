<?php

    namespace CQRS\Core\Domain\Model\Product;

    interface ProductModelInterface {

        public function getProductId(): string;

        public function getUserId(): string;

        public function getPublicationDate(): string;

        public function getTitle(): string;

        public function getDescription(): string;
        
        public function index(array $products): void;

        public function storeSuccess(): void;

        public function storeError(): void;

        public function showSuccess(array $product_info): void;

        public function showError(): void;

        public function editSuccess(array $product_info): void;

        public function editError(): void;

        public function updateSuccess(): void;

        public function updateError(): void;

        public function deleteSuccess(): void;
        
        public function deleteError(): void;

    }