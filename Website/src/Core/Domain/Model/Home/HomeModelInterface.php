<?php

    namespace CQRS\Core\Domain\Model\Home;
    
    interface HomeModelInterface {
        
        public function index(): void;

        public function notFound(): void;
        
    }