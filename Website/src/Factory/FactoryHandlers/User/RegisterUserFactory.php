<?php

    namespace CQRS\Factory\FactoryHandlers\User;

    use CQRS\Core\Domain\Model\User\UserService;
    use CQRS\App\Command\User\RegisterUser\RegisterUserCommand;
    use CQRS\App\Command\User\RegisterUser\RegisterUserCommandHandler;
    use CQRS\Core\Infrastructure\Repository\UserRepository\UserRepository;
    use CQRS\Core\Infrastructure\Database\Database;

    final class RegisterUserFactory {

        public static function register(string $request, Database $db) {

            $user_repository = new UserRepository($db);
            $user_service = new UserService($user_repository);

            $credentails = [
                'name' => $_POST['name'] ?? null,
                'surname' => $_POST['surname'] ?? null,
                'email' => $_POST['email'] ?? null,
                'phone' => $_POST['phone'] ?? null,
                'password' => $_POST['password'] ?? null,
                'city' => $_POST['city'] ?? null
            ];

            $register_command = new RegisterUserCommand($credentails, $request);

            return new RegisterUserCommandHandler($user_service, $register_command);

        }

    }