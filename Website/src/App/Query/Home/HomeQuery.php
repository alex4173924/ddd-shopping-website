<?php

    namespace CQRS\App\Query\Home;

    final class HomeQuery {

        private $request;

        public function __construct(string $request) {

            $this->request = $request;

        }

        public function getHomeRequest(): string {
            
            return $this->request;

        }

    }