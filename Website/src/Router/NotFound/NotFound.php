<?php

    namespace CQRS\Router\NotFound;

    use CQRS\Factory\Factory;

    final class NotFound {

        public static function routeNotFound(): void {

            $path = [
                'controller' => 'HomeQueryHandler',
                'request' => ''
            ];
    
            $factory = new Factory($path);
    
            $controller = $factory->createPath();
            $controller->handle();

        }

    }