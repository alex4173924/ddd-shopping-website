<?php

    namespace CQRS\App\Query\User\LogoutUser;

    final class LogoutUserQuery {

        private $request;

        public function __construct(string $request) {

            $this->request = $request;

        }

        public function getRequest() {

            return $this->request;
            
        }

    }