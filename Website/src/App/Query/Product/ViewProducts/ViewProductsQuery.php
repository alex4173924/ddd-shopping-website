<?php

    namespace CQRS\App\Query\Product\ViewProducts;

    final class ViewProductsQuery {

        private $request;

        public function __construct(string $request) {
            
            $this->request = $request;
        
        }

        public function getRequest() {

            return $this->request;

        }


    }