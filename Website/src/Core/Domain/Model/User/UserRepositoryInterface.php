<?php

    namespace CQRS\Core\Domain\Model\User;

    interface UserRepositoryInterface {

        public function findById(string $id): ?User;

        public function findByEmail(string $email): ?User;

        public function checkCredentials(array $credentials): bool;

        public function update(array $user, string $id): void;

        public function save(User $user): void;

    }