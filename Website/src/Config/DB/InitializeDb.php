<?php

    namespace CQRS\Config\DB;

    use mysqli;

    final class InitializeDb {

        private $host;
        private $username;
        private $password;
        private $db_name;

        public function __construct() {

            $this->host = ConfigDb::get_db_val('host');
            $this->username = ConfigDb::get_db_val('username');
            $this->password = ConfigDb::get_db_val('password');
            $this->db_name = ConfigDb::get_db_val('db_name');
        
        }

        public function init() {
        
            $conn = new mysqli($this->host, $this->username, $this->password);

            if ($conn->connect_error) {
                die('Unable to connect to the database: ' . $conn->connect_error);
            }

            $sql = "CREATE DATABASE IF NOT EXISTS {$this->db_name}";
            
            if (!$conn->query($sql)) {
                die('Unable to create database: ' . $this->db_name . ' - ' . $conn->error);
            }

            $this->createTables();
        
        }

        public function createTables() {
        
            $conn = new mysqli($this->host, $this->username, $this->password, $this->db_name);

            if ($conn->connect_error) {
                die('Unable to connect to the database: ' . $conn->connect_error);
            }

            $usersTable = "CREATE TABLE IF NOT EXISTS users (
                id INT(11) AUTO_INCREMENT PRIMARY KEY,
                name VARCHAR(255) NOT NULL,
                surname VARCHAR(255) NOT NULL,
                email VARCHAR(255) NOT NULL,
                phone VARCHAR(255) NOT NULL,
                city VARCHAR(255) NOT NULL,
                password VARCHAR(255) NOT NULL
            )";

            if (!$conn->query($usersTable)) {
                die('Unable to create table: users - ' . $conn->error);
            }

            $productsTable = "CREATE TABLE IF NOT EXISTS products (
                id INT(11) AUTO_INCREMENT PRIMARY KEY,
                user_id INT(11) NOT NULL,
                publication_date DATE NOT NULL,
                title VARCHAR(255) NOT NULL,
                description VARCHAR(255) NOT NULL
            )";

            if (!$conn->query($productsTable)) {
                die('Unable to create table: products - ' . $conn->error);
            }

        }
    }
