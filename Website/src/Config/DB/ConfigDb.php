<?php

    namespace CQRS\Config\DB;

    final class ConfigDb {

        private static $db_vals = null;

        private static function init_vals() {

            self::$db_vals = [

                'host' => 'localhost',
                'username' => 'root',
                'password' => '',
                'db_name' => 'shopping_db'

            ];

        }

        public static function get_db_val($key) {

            self::init_vals();
            return self::$db_vals[$key] ?? ''; 

        }
    }