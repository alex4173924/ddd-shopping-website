<?php

    namespace CQRS\Core\Domain\Model\User;

    use CQRS\Core\Infrastructure\HashPassword\HashPassword;

    final class User implements UserModelInterface {

        private $id;
        private $name;
        private $surname;
        private $email;
        private $phone;
        private $city;
        private $password;

        public function __construct(array $credentials) {

            $this->id = $credentials['id'] ?? '';
            $this->name = $credentials['name'] ?? '';
            $this->surname = $credentials['surname'] ?? '';
            $this->email = $credentials['email'] ?? '';
            $this->phone = $credentials['phone'] ?? '';
            $this->city = $credentials['city'] ?? '';
            $this->password = $credentials['password'] ?? '';

        }

        #[\Override]
        public function getId(): string {

            return $this->id;

        }

        #[\Override]
        public function getName(): string {

            return $this->name;

        }

        #[\Override]
        public function getSurname(): string {

            return $this->surname;

        }

        #[\Override]
        public function getEmail(): string {

            return $this->email;

        }

        #[\Override]
        public function getPhone(): string {

            return $this->phone;

        }

        #[\Override]
        public function getCity(): string {
            
            return $this->city;

        }

        #[\Override]
        public function getPassword(): string {

            return $this->password;

        }

        #[\Override]
        public function login(): void {

            $success_message = 'You have logged into your profile !';
            $query_string = 'success=' . urlencode($success_message);

            if(session_status() !== PHP_SESSION_ACTIVE) {
                
                session_start();
            
            }

            $_SESSION['user_id'] = $this->getId();

            header('Location:src/View/home.php?' . $query_string);
            exit();

        }

        #[\Override]
        public function loginError(): void {

            $error_message = 'Invalid credentials';
            $query_string = 'error=' . urlencode($error_message);

            header('Location:src/View/Users/Auth/login.php?' . $query_string);
            exit();

        }

        #[\Override]
        public function register(): void {

            $success_message = 'You have registered your profile successfully! Log in with your credentials.';
            $query_string = 'success=' . urlencode($success_message);

            header('Location:src/View/Users/Auth/login.php?' . $query_string);
            exit();

        }

        #[\Override]
        public function registerError(): void {

            $error_message = 'Inavlid credentials. Email or Phone already taken!';
            $query_string = 'error=' . urlencode($error_message);

            header('Location:src/View/Users/Auth/register.php?' . $query_string);
            exit();

        }

        #[\Override]
        public function editSuccess(array $user_data): void {

            $user_data_json = json_encode($user_data);
            
            $query_string = 'user_data=' . urlencode($user_data_json);

            header('Location:src/View/Users/edit.php?' . $query_string);
            exit();

        }

        #[\Override]
        public function editError(): void {

            $error_message = 'No such profile located!';
            $controller = 'HomeQueryHandler';
            $request = 'index';
            $method = 'handle';

            $query_string = 'controller=' . urlencode($controller) . '&request=' . urlencode($request) . '&method=' . urlencode($method) . '&error=' . urlencode($error_message);
            
            header('Location:index.php?' . $query_string);
            exit();

        }

        #[\Override]
        public function updateSuccess(): void {

            $success_message = 'You have updated your profile successfully!';

            $controller = 'HomeQueryHandler';
            $request = 'index';
            $method = 'handle';

            $query_string = 'controller=' . urlencode($controller) . '&request=' . urlencode($request) . '&method=' . urlencode($method) . '&success=' . urlencode($success_message);

            header('Location:index.php?' . $query_string);
            exit();
        }

        #[\Override]
        public function updateError(): void {

            $error_message = 'Somethig went wrong with your request!';
            $controller = 'EditUserQueryHandler';
            $request = 'edit';
            $method = 'handle';

            $query_string = 'controller=' . urlencode($controller) . '&request=' . urlencode($request) . '&method=' . urlencode($method) . '&error=' . urlencode($error_message);
            
            header('Location:index.php?' . $query_string);
            exit();

        }

        #[\Override]
        public function logoutSuccess(): void {

            $_SESSION['user_id'] = null;
            $success_message = 'You have logged out of your profile!';
            $controller = 'HomeQueryHandler';
            $request = 'index';
            $method = 'handle';

            $query_string = 'controller=' . urlencode($controller) . '&request=' . urlencode($request) . '&method=' . urlencode($method) . '&success=' . urlencode($success_message);
            
            header('Location:index.php?' . $query_string);
            exit();

        }

        #[\Override]
        public function logoutError(): void {

            $error_message = 'No such profile located!';
            $controller = 'HomeQueryHandler';
            $request = 'index';
            $method = 'handle';

            $query_string = 'controller=' . urlencode($controller) . '&request=' . urlencode($request) . '&method=' . urlencode($method) . '&error=' . urlencode($error_message);
            
            header('Location:index.php?' . $query_string);
            exit();

        }

    }