<?php

    namespace CQRS\Factory\FactoryHandlers\User;

    use CQRS\Core\Domain\Model\User\UserService;
    use CQRS\App\Query\User\LogoutUser\LogoutUserQuery;
    use CQRS\App\Query\User\LogoutUser\LogoutUserQueryHandler;
    use CQRS\Core\Infrastructure\Repository\UserRepository\UserRepository;
    use CQRS\Core\Infrastructure\Database\Database;

    final class LogoutUserFactory {

        public static function logout(string $request, Database $db) {

            $user_repository = new UserRepository($db);
            $user_service = new UserService($user_repository);
            $logout_query = new LogoutUserQuery($request);

            return new LogoutUserQueryHandler($user_service, $logout_query);

        }
    }