<?php

    namespace CQRS\App\Query\Product\EditProduct;

    use CQRS\Core\Domain\Model\Product\ProductService;

    final class EditProductQueryHandler {

        private $product_service;
        private $product_query;

        public function __construct(ProductService $product_service, EditProductQuery $product_query) {

            $this->product_service = $product_service;
            $this->product_query = $product_query;

        }

        public function handle() {

            $request = $this->product_query->getRequest();

            $product_data = [
                'id' => $this->product_query->getId()
            ];

            $this->product_service->execute($product_data, $request);

        }
    }