<?php

    namespace CQRS\Factory\FactoryHandlers\User;

    use CQRS\Core\Domain\Model\User\UserService;
    use CQRS\App\Command\User\UpdateUser\UpdateUserCommand;
    use CQRS\App\Command\User\UpdateUser\UpdateUserCommandHandler;
    use CQRS\Core\Infrastructure\Repository\UserRepository\UserRepository;
    use CQRS\Core\Infrastructure\Database\Database;

    final class UpdateUserFactory {

        public static function update(string $request, Database $db) {

            $user_repository = new UserRepository($db);
            $user_service = new UserService($user_repository);       
            $user_data = [

                'name' => $_POST['edit_name'] ?? null,
                'surname' => $_POST['edit_surname'] ?? null,
                'email' => $_POST['edit_email'] ?? null,
                'phone' => $_POST['edit_phone'] ?? null,
                'city' => $_POST['edit_city'] ?? null

            ];
            $user_command = new UpdateUserCommand($user_data, $request);

            return new UpdateUserCommandHandler($user_service, $user_command);

        }

    }