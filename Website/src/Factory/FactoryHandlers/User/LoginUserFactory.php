<?php

    namespace CQRS\Factory\FactoryHandlers\User;

    use CQRS\Core\Domain\Model\User\UserService;
    use CQRS\App\Query\User\LoginUser\LoginUserQueryHandler;
    use CQRS\App\Query\User\LoginUser\LoginUserQuery;
    use CQRS\Core\Infrastructure\Repository\UserRepository\UserRepository;
    use CQRS\Core\Infrastructure\Database\Database;

    final class LoginUserFactory {

        public static function login(string $request, Database $db) {

            $user_repository = new UserRepository($db);
            $userService = new UserService($user_repository);
            $email = isset($_POST['email']) ? $_POST['email'] : null;
            $password = isset($_POST['password']) ? $_POST['password'] : null;
            $query = new LoginUserQuery($email, $password, $request);
                    
            return new LoginUserQueryHandler($userService, $query);

        }
    }