<?php

    namespace CQRS\App\Query\User\EditUser;

    use CQRS\Core\Domain\Model\User\UserService;

    final class EditUserQueryHandler {

        private $user_service;
        private $user_query;

        public function __construct(UserService $user_service, EditUserQuery $user_query) {

            $this->user_service = $user_service;
            $this->user_query = $user_query;

        }

        public function handle() {

            $request = $this->user_query->getRequest();
            $this->user_service->execute([], $request);

        }

    }