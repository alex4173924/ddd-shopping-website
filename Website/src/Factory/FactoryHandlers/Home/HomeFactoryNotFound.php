<?php

    namespace CQRS\Factory\FactoryHandlers\Home;

    use CQRS\Core\Domain\Model\Home\Home;
    use CQRS\Core\Domain\Model\Home\HomeService;
    use CQRS\App\Query\Home\HomeQuery;
    use CQRS\App\Query\Home\HomeQueryHandler;

    final class HomeFactoryNotFound {

        public static function notFound() {

            $home = new Home();
            $home_service = new HomeService($home);
            $home_query = new HomeQuery('');

            return new HomeQueryHandler($home_service, $home_query);

        }

    }