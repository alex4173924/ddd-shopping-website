<?php

    namespace CQRS\Factory\FactoryHandlers\Product;

    use CQRS\Core\Domain\Model\Product\ProductService;
    use CQRS\App\Query\Product\ViewProducts\ViewProductsQuery;
    use CQRS\App\Query\Product\ViewProducts\ViewProductsQueryHandler;
    use CQRS\Core\Infrastructure\Repository\ProductRepository\ProductRepository;
    use CQRS\Core\Infrastructure\Database\Database;

    final class ViewProductFactory {

        public static function view(string $request, Database $db) {

            $product_respository = new ProductRepository($db);
            $product_service = new ProductService($product_respository);
            $product_query = new ViewProductsQuery($request);

            return new ViewProductsQueryHandler($product_service, $product_query);

        }

    }