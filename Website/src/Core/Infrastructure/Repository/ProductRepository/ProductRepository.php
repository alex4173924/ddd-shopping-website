<?php

    namespace CQRS\Core\Infrastructure\Repository\ProductRepository;

    use CQRS\Core\Domain\Model\Product\ProductRepositoryInterface;
    use CQRS\Core\Domain\Model\Product\Product;
    use CQRS\Core\Infrastructure\Database\Database;

    final class ProductRepository implements ProductRepositoryInterface {

        private $db;

        public function __construct(Database $db) {
            
            $this->db = $db;
        
        }

        #[\Override]
        public function findById(string $id): ?Product {

            $table = 'products';

            $sql = "SELECT 
                        id,
                        user_id,
                        publication_date,
                        title,
                        description
                    FROM $table
                    WHERE id='$id'";

            $result = $this->db->query($sql);

            if($result->num_rows > 0) {
                $product = $result->fetch_assoc();

                return new Product([
                    'id' => $product['id'],
                    'user_id' => $product['user_id'],
                    'publication_date' => $product['publication_date'],
                    'title' => $product['title'],
                    'description' => $product['description']
                ]);
            }

            return new Product([]);

        }

        #[\Override]
        public function getAllProducts(): array {

            $table = 'products';

            $sql = "SELECT *
                    FROM $table
                        ORDER BY id ASC";

            $result = $this->db->query($sql);

            $products = [];

            if($result->num_rows > 0) {

                $products = $result->fetch_all(MYSQLI_ASSOC);

            }

            return $products;
        }

        #[\Override]
        public function checkProduct(string $title): bool {

            $table = 'products';

            $sql = "SELECT
                        user_id,
                        publication_date,
                        title,
                        description
                    FROM $table
                    WHERE title='$title'";

            $result = $this->db->query($sql);

            if($result->num_rows > 0) {

                return false;

            }

            return true;

        }
        
        #[\Override]
        public function update(array $product): void {

            $table = 'products';

            $sql = "UPDATE $table
                    SET
                        publication_date='{$product['publication_date']}',
                        title='{$product['title']}',
                        description='{$product['description']}'
                    WHERE id={$product['id']}";

            if(!$this->db->query($sql)) {

                die('Unable to update record with id: ' . $product->getId());

            }
        }

        #[\Override]
        public function save(Product $product): void {

            $table = 'products';

            $sql = "INSERT
                        INTO $table(
                            user_id,
                            publication_date,
                            title,
                            description
                        ) VALUES(
                            '{$product->getUserId()}',
                            '{$product->getPublicationDate()}',
                            '{$product->getTitle()}',
                            '{$product->getDescription()}'
                        )";

            if(!$this->db->query($sql)) {
                die('Unable to insert record to the database!');
            }

        }

        #[\Override]
        public function delete(string $id): void {

            $table = 'products';

            $sql = "DELETE
                    FROM $table
                    WHERE id=$id";

            if(!$this->db->query($sql)) {
                
                die('Unable to delete product with id: ' . $id);
                
            }

        }

    }