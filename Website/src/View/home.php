<?php 
    require_once "includes/header.php";     

?>



    <h1 style="display: flex; justify-content: center; align-items: center; height: 30vh; color:darkslategray">Welcome to the Home page</h1>
    <hr style="border-color: white; border-width: 5px">

    <div style="display: flex; justify-content: center; align-items: center; color:blanchedalmond; height: 20vh;">
    <?php
        if(!empty($_GET['success'])) {
            echo "<h4 style='color: green'>" . $_GET['success'] . "</h4><br>";
            $_GET['success'] = null;
        } elseif(!empty($_GET['error'])) {
            echo "<h4 style='color: red'>" . $_GET['error'] . "</h4><br>";
            $_GET['error'] = null;
        }
    ?>
    </div> 

    <div style="display: flex; justify-content: center; align-items: center; height: 5vh;">

    <?php if(isset($_SESSION['user_id'])) {?>

            <div>
                <a href="../../index.php?controller=ViewProductsQueryHandler&request=index&method=handle"><button class="btn btn-primary" style="margin-right: 5px; padding: 15px 20px; font-size: 20px; border-radius: 20px;">View Producst</button></a>
                <a href="../../index.php?controller=EditUserQueryHandler&request=edit&method=handle"><button class="btn btn-primary" style="margin-right: 5px; padding: 15px 20px; font-size: 20px; border-radius: 20px;">Edit Profile</button></a>
                <a href="../../index.php?controller=LogoutUserQueryHandler&request=logout&method=handle"><button class="btn btn-primary" style="margin-right: 5px; padding: 15px 20px; font-size: 20px; border-radius: 20px;">Logout</button></a>
            </div>

        <?php } else { ?>
            <div>
                <a href="Users/Auth/login.php"><button class="btn btn-primary" style="margin-right: 5px; padding: 15px 20px; font-size: 20px; border-radius: 20px;">Login</button></a>
                <a href="Users/Auth/register.php"><button class="btn btn-primary" style="margin-right: 5px; padding: 15px 20px; font-size: 20px; border-radius: 20px;">Register</button></a>
            </div>
        <?php } ?>

    </div>


<?php require_once "includes/footer.php"; ?>