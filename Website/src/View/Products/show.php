<?php
    
    require_once '../includes/header.php';

    if(isset($_SESSION['user_id'])) {

        $product = isset($_GET['product']) ? json_decode($_GET['product']) : null;

        if($product !== null) {

?>

    <div>
        <h1 class="product_h1">'<?= $product->title ?>':</h1><br>
    </div>


    <div>
        <table>
            <thead>
                <tr>
                    <th>Publication date</th>
                    <th>Title</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?= $product->publication_date ?></td>
                    <td><?= $product->title ?></td>
                    <td><?= $product->description ?></td>
                </tr> 
            </tbody>
        </table>
    </div>

    <br><br>
    <hr class="hr">
    <div>
        <h3 class="product_h2">Seller details:</h3><br>
    </div>


    <div>
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>City</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php 
                        if($product->user_name !== '') {
                            echo $product->user_name; 
                        } else {
                            echo '--';
                        }
                    ?></td>
                    <td><?php 
                        if($product->user_surname !== '') {
                            echo $product->user_surname;
                        } else {
                            echo '--';
                        }
                    ?></td>
                    <td><?php 
                        if($product->user_email !== '') {
                            echo $product->user_email;
                        } else {
                            echo '--';
                        }
                    ?></td>
                    <td><?php 
                        if($product->user_phone !== '') {
                            echo $product->user_phone;
                        } else {
                            echo '--';
                        }
                    ?></td>
                    <td><?php
                        if($product->user_city !== '') {
                            echo $product->user_city;
                        } else {
                            echo '--';
                        }
                    ?></td>
                </tr> 
            </tbody>
        </table>
    </div>



    <div style="display: flex; justify-content: center; align-items: center; height: 15vh;">
        <a href="../../../index.php?controller=ViewProductsQueryHandler&request=index&method=handle"><button style=" width: 100%; padding: 10px;padding-right: 50px;
                padding-left: 50px; background-color: #297fb1;
                color: white; border: none; margin-top: 5px; border-radius: 20px;">Back</button></a>
    </div>


<?php }
    
    } else { ?>
    <h1 style="display: flex; justify-content: center; align-items: center; height: 15vh; color:blanchedalmond;">Login to your profile first!</h1>
<?php } ?>

<?php 
    require_once "../includes/footer.php"; 
?>
