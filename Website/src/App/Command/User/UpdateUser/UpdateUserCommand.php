<?php

    namespace CQRS\App\Command\User\UpdateUser;

    final class UpdateUserCommand {

        private $request;
        private $name;
        private $surname;
        private $email;
        private $phone;
        private $city;

        public function __construct(array $user_data, string $request) {

            $this->request = $request;
            $this->name = $user_data['name'] ?? '';
            $this->surname = $user_data['surname'] ?? '';
            $this->email = $user_data['email'] ?? '';
            $this->phone = $user_data['phone'] ?? '';
            $this->city = $user_data['city'] ?? '';

        }

        public function getRequest() {

            return $this->request;

        }

        public function getUserData() {

            return [

                'name' => $this->name,
                'surname' => $this->surname,
                'email' => $this->email,
                'phone' => $this->phone,
                'city' => $this->city

            ];

        }

    }
