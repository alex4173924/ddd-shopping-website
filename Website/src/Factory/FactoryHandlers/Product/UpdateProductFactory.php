<?php

    namespace CQRS\Factory\FactoryHandlers\Product;

    use CQRS\Core\Domain\Model\Product\ProductService;
    use CQRS\App\Command\Product\UpdateProduct\UpdateProductCommand;
    use CQRS\App\Command\Product\UpdateProduct\UpdateProductCommandHandler;
    use CQRS\Core\Infrastructure\Repository\ProductRepository\ProductRepository;
    use CQRS\Core\Infrastructure\Database\Database;

    final class UpdateProductFactory {

        public static function update(string $request, Database $db) {

            $product_respository = new ProductRepository($db);
            $product_service = new ProductService($product_respository);
            $product = [

                'id' => $_POST['id'] ?? null,
                'publication_date' => $_POST['edit_publication_date'] ?? null,
                'title' => $_POST['edit_title'] ?? null,
                'description' => $_POST['edit_description'] ?? null

            ];
            $product_command = new UpdateProductCommand($product, $request);

            return new UpdateProductCommandHandler($product_service, $product_command);

        }
    }