<?php

    namespace CQRS\App\Query\User\LoginUser;

    use CQRS\Core\Domain\Model\User\UserService;

    final class LoginUserQueryHandler {

        private $user_service;
        private $user_query;

        public function __construct(UserService $user_service, LoginUserQuery $user_query) {

            $this->user_service = $user_service;
            $this->user_query = $user_query;

        }

        public function handle() {

            $request = $this->user_query->getRequest();

            $user_data = $this->user_query->getUserData();

            $this->user_service->execute($user_data, $request);
        }

    }