<?php

    namespace CQRS\App\Command\Product\UpdateProduct;

    final class UpdateProductCommand {

        private $request;
        private $id;
        private $publication_date;
        private $title;
        private $description;

        public function __construct(array $product, string $request) {

            $this->request = $request;
            $this->id = $product['id'];
            $this->publication_date = $product['publication_date'];
            $this->title = $product['title'];
            $this->description = $product['description'];

        }

        public function getRequest() {

            return $this->request;

        }

        public function getProductData() {

            return [

                'id' => $this->id,
                'publication_date' => $this->publication_date,
                'title' => $this->title,
                'description' => $this->description

            ];
            
        }

    }