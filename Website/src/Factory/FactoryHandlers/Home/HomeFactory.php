<?php

    namespace CQRS\Factory\FactoryHandlers\Home;

    use CQRS\Core\Domain\Model\Home\Home;
    use CQRS\Core\Domain\Model\Home\HomeService;
    use CQRS\App\Query\Home\HomeQuery;
    use CQRS\App\Query\Home\HomeQueryHandler;

    final class HomeFactory {
        
        public static function home(string $request) {

            $home = new Home();
            $home_service = new HomeService($home);
            $home_query = new HomeQuery($request);

            return new HomeQueryHandler($home_service, $home_query);

        }

    }