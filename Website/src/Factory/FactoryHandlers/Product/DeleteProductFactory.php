<?php

    namespace CQRS\Factory\FactoryHandlers\Product;

    use CQRS\Core\Domain\Model\Product\ProductService;
    use CQRS\App\Command\Product\DeleteProduct\DeleteProductCommand;
    use CQRS\App\Command\Product\DeleteProduct\DeleteProductCommandHandler;
    use CQRS\Core\Infrastructure\Repository\ProductRepository\ProductRepository;
    use CQRS\Core\Infrastructure\Database\Database;

    final class DeleteProductFactory {

        public static function delete(string $request, Database $db) {

            $product_respository = new ProductRepository($db);
            $product_service = new ProductService($product_respository);
            $id = $_GET['id'] ?? '';
            $product_command = new DeleteProductCommand($id, $request);

            return new DeleteProductCommandHandler($product_service, $product_command);

        }

    }