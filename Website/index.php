<?php

    require_once __DIR__ . '/vendor/autoload.php';

    use CQRS\Router\Router;
    use CQRS\Config\DB\InitializeDb;

    $db = new InitializeDb;
    $db->init();

    $router = new Router();

    $route = isset($_GET['controller']) ? $_GET : $_GET = ['controller' => 'HomeQueryHandler', 'request' => 'index', 'method' => 'handle'];

    $router->makeRoute($route);

?>