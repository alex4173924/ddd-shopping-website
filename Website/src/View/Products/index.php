<?php 
    require_once '../includes/header.php';   
?>


<?php if(isset($_SESSION['user_id'])) {?>

<h1 class="product_h1">Products</h1>

<div class="session_messages">
<?php  

    if(!empty($_GET['success'])) {
        echo "<h4 style='color: green'>" . $_GET['success'] . "</h4><br>";
        $_GET['success'] = null;
    } elseif(!empty($_GET['error'])) {
        echo "<h4 style='color: red'>" . $_GET['error'] . "</h4><br>";
        $_GET['error'] = null;
    } else {
        echo "<h4>Browse through the products!</h4><br>";
    }

    $products = isset($_GET['products']) ? json_decode($_GET['products']) : null;

?>

</div>

<div>
    <h2 class="product_h2">Available products:</h2><br>
</div>


<div>
    <?php if(!empty($products) && $products != null) { ?>
        <table>
            <thead>
                <tr>
                    <th>Publication date</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                    <?php foreach($products as $key => $product) {?>                

                        <td><?= $product->publication_date ?></td>
                        <td><?= $product->title ?></td>
                        <td><?= $product->description ?></td>
                        <td>
                            <div class="index_product_buttons">
                                <a href="../../../index.php?controller=ShowProductQueryHandler&request=show&method=handle&id=<?=$product->id?>"><button style="border-radius: 20px; background-color: #297fb1; color: white;">Show</button></a>
                                <a href="../../../index.php?controller=EditProductQueryHandler&request=edit&method=handle&id=<?=$product->id?>"><button style="border-radius: 20px; background-color: #297fb1; color: white;">Edit</button></a>
                                <a href="../../../index.php?controller=DeleteProductCommandHandler&request=delete&method=handle&id=<?=$product->id?>"><button style="border-radius: 20px; background-color: #297fb1; color: white;">Delete</button></a>
                            </div>
                        </td>
                    </tr>

                <?php } 
    } else { ?>
        <tr>
            <td colspan="5"><h4 style="color:cornflowerblue;">No products listed yet.</h4></td>
        </tr>
        <?php } ?>
    </tbody>
</table>
</div>


<a style="display: flex; justify-content: center; border: none; align-items: center; text-decoration: none; border-radius: 20px;" href="create.php"><button class="new_product_btn">Create new product:</button></a>


<div style="display: flex; justify-content: center; align-items: center; height: 15vh;">
        <a href="../home.php"><button style=" width: 100%; padding: 10px;padding-right: 50px;
                padding-left: 50px; background-color: #297fb1;
                color: white; border: none; margin-top: 5px; border-radius: 20px;">Back</button></a>
</div>

<?php } else { ?>
    <h1 style="display: flex; justify-content: center; align-items: center; height: 15vh; color:blanchedalmond;">Login to your profile first!</h1>

<?php } ?>

<?php require_once "../includes/footer.php"; ?>