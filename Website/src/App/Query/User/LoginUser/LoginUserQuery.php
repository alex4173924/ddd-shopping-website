<?php

    namespace CQRS\App\Query\User\LoginUser;

    final class LoginUserQuery {

        private $request;
        private $email;
        private $password;

        public function __construct($email, $password, $request) {

            $this->request = $request;
            $this->email = $email;
            $this->password = $password;

        }

        public function getRequest() {

            return $this->request;
            
        }

        public function getUserData() {

            return [

                'email' => $this->email,
                'password' => $this->password

            ];

        }
        
    }