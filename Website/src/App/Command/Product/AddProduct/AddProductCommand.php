<?php

    namespace CQRS\App\Command\Product\AddProduct;

    final class AddProductCommand {

        private $request;
        private $user_id;
        private $publicationd_date;
        private $title;
        private $description;

        public function __construct(array $product_data, string $request) {

            $this->request = $request;
            $this->user_id = $product_data['user_id'] ?? '';
            $this->publication_date = $product_data['publication_date'] ?? '';
            $this->title = $product_data['title'] ?? '';
            $this->description = $product_data['description'] ?? '';

        }

        public function getRequest() {

            return $this->request;

        }

        public function getProductData() {

            return [

                'user_id' => $this->user_id,
                'publication_date' => $this->publication_date,
                'title' => $this->title,
                'description' => $this->description

            ];

        }

    }