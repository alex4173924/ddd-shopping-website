<?php

    namespace CQRS\Core\Domain\Model\Product;

    use CQRS\Core\Domain\Model\User\UserRepositoryInterface;

    final class ProductService {

        private $product_repository;
        private $user_repository;

        public function __construct(ProductRepositoryInterface $product_repository, UserRepositoryInterface $user_repository = null) {

            $this->product_repository = $product_repository;
            $this->user_repository = $user_repository;

        }

        public function execute(array $product_data, string $query) {

            switch($query) {

                case 'index':

                    $all_products = $this->product_repository->getAllProducts();
                    $products = new Product([]);
                    $products->index($all_products);
                
                    break;

                case 'addItem':
                    
                    $product = new Product($product_data);

                    if($this->product_repository->checkProduct($product_data['title'])) {

                        $this->product_repository->save($product);
                        $product->storeSuccess();

                    }

                    $product->storeError();
                    break;

                case 'show':

                    $product = $this->product_repository->findById($product_data['id']);

                    if($product->getProductId() !== '') {

                        $user = $this->user_repository->findById($product->getUserId());

                        $product_info = [

                            'publication_date' => $product->getPublicationDate(),
                            'title' => $product->getTitle(),
                            'description' => $product->getDescription(),
                            'user_name' => $user->getName(),
                            'user_surname' => $user->getSurname(),
                            'user_email' => $user->GetEmail(),
                            'user_phone' => $user->GetPhone(),
                            'user_city' => $user->GetCity()

                        ];

                        $product->showSuccess($product_info);

                    }

                    $product->showError();
                    break;

                case 'edit':

                    $product = $this->product_repository->findById($product_data['id']);

                    if($product->getProductId() !== '') {

                        $product_info = [

                            'id' => $product->getProductId(),
                            'publication_date' => $product->getPublicationDate(),
                            'title' => $product->getTitle(),
                            'description' => $product->getDescription(),

                        ];

                        $product->editSuccess($product_info);
                    }

                    $product->editError();

                    break;

                case 'update':

                    $product = $this->product_repository->findById($product_data['id']);

                    if($product->getProductId() !== '') {

                        $this->product_repository->update($product_data);
                        $product->updateSuccess();

                    }

                    $product->updateError();

                    break;

                case 'delete':

                    $product = $this->product_repository->findById($product_data['id']);

                    if($product->getProductId() !== '') {

                        $this->product_repository->delete($product_data['id']);
                        $product->deleteSuccess();

                    }

                    $product->deleteError();

                    break;
            }

        }

    }