<?php

    namespace CQRS\App\Command\User\RegisterUser;

    use CQRS\Core\Domain\Model\User\UserService;

    final class RegisterUserCommandHandler {

        private $user_service;
        private $register_command;

        public function __construct(UserService $user_service, RegisterUserCommand $register_command) {

            $this->user_service = $user_service;
            $this->register_command = $register_command;
        
        }

        public function handle() {

            $request = $this->register_command->getRequest();

            $user_data = $this->register_command->getUserData();

            $this->user_service->execute($user_data, $request);

        }

    }