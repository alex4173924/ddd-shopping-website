<?php

    namespace CQRS\Core\Infrastructure\Database;

    use mysqli;
    use CQRS\Config\DB\ConfigDb;

    final class Database extends mysqli {

        private $host;
        private $username;
        private $password;
        private $db_name;

        public function __construct() {

            $this->host = ConfigDb::get_db_val('host');
            $this->username = ConfigDb::get_db_val('username');
            $this->password = ConfigDb::get_db_val('password');
            $this->db_name = ConfigDb::get_db_val('db_name');

            $conn = parent::__construct($this->host, $this->username, $this->password, $this->db_name);

            if(!$conn) {
                die('Unable to connect to the database!');
            }

        }

        public function query(string $sql, int $result_mode = MYSQLI_STORE_RESULT) {

            return parent::query($sql, $result_mode);

        }

    }