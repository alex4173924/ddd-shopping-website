<?php

    namespace CQRS\App\Query\User\EditUser;

    final class EditUserQuery {

        private $request;

        public function __construct(string $request) {

            $this->request = $request;

        }

        public function getRequest() {

            return $this->request;

        }

    }
