<?php 

require_once '../includes/header.php';

    if(isset($_SESSION['user_id'])) {

        if(!empty($_GET['error'])) {
            
            echo "<h4 style='color: red'>" . $_GET['error'] . "</h4><br>";
            $_GET['error'] = null;
        
        } else {

            echo "<h1 class='product_h1' style='margin-bottom: 100px;'>Edit item:</h1>";
            
        } 
        
        $product = isset($_GET['product']) ? json_decode($_GET['product']) : null;

        if($product !== null) {

?>

<div class="product_div" style="margin-bottom: 100px;;">
    <form method="POST" action="../../../index.php?controller=UpdateProductCommandHandler&request=update&method=handle" enctype="multipart/form-data">
        
        <input type="hidden" name="id" value="<?=$product->id?>">

        <label for="edit_publication_date">Publication date:</label><br>
        <input type="date" name="edit_publication_date" id="edit_publication_date" value="<?= $product->publication_date ?>"><br><br>

        <label for="edit_title">Title:</label><br>
        <input type="text" name="edit_title" id="edit_title" value="<?= $product->title ?>"><br><br>

        <label for="edit_description">Description:</label><br>
        <input type="text" name="edit_description" id="edit_description" value="<?= $product->description ?>"><br><br>

        <input type="submit" style=" margin-top: 5px; display: block; width: 100%; padding: 10px; background-color: #4CAF50; color: white;
                            border: none; border-radius: 20px;" >

    </form>

</div>

    <?php } else { ?>
        <h1 style="display: flex; justify-content: center; align-items: center; height: 15vh; color:blanchedalmond;">The item you are looking for was not found!</h1>
    <?php }
    
    } else { ?>

    <h1 style="display: flex; justify-content: center; align-items: center; height: 15vh; color:blanchedalmond;">Login to your profile first!</h1>
<?php } ?>

<div style="display: flex; justify-content: center; align-items: center; height: 15vh;">
    <a href="../../../index.php?controller=ViewProductsQueryHandler&request=index&method=handle"><button style=" width: 100%; padding: 10px;padding-right: 50px;
            padding-left: 50px; background-color: #297fb1;
            color: white; border: none; margin-top: 5px; border-radius: 20px;">Back</button></a>
</div>



<?php require_once '../includes/footer.php'; ?>
