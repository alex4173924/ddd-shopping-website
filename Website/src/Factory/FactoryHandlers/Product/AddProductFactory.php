<?php

    namespace CQRS\Factory\FactoryHandlers\Product;

    use CQRS\Core\Domain\Model\Product\ProductService;
    use CQRS\App\Command\Product\AddProduct\AddProductCommand;
    use CQRS\App\Command\Product\AddProduct\AddProductCommandHandler;
    use CQRS\Core\Infrastructure\Repository\ProductRepository\ProductRepository;
    use CQRS\Core\Infrastructure\Database\Database;

    final class AddProductFactory {

        public static function add(string $request, Database $db) {

            $product_respository = new ProductRepository($db);
            $product_service = new ProductService($product_respository);

            $product_data = [
                
                'user_id' => $_POST['user_id'] ?? null,
                'publication_date' => $_POST['publication_date'] ?? null,
                'title' => $_POST['title'] ?? null,
                'description' => $_POST['description'] ?? null
            
            ];

            $product_command = new AddProductCommand($product_data, $request);

            return new AddProductCommandHandler($product_service, $product_command);

        }

    }